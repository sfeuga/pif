#!/usr/bin/env bash

blob_url="https://gitlab.com/sfeuga/pif/-/raw/master/Sources";
total_file_to_dl=$(wc -l urls.txt | awk '{print $1}');
download_file_number=0;

sed -i.old "/- \[/d" ../README.md;

cd ../Sources/

while read line; do

    download_file_number=$((download_file_number + 1));
    echo "Download $download_file_number/$total_file_to_dl: $line...";

    file=$(echo "$line" | rev | awk -F "/" '{print $1}' | rev);
    referer=$(echo "https://$(echo "$line" | awk -F "/" '{print $3}')");

    wget -c --progress=dot --referer="$referer" -O ../Sources/$file "$line";

done < ../Tools/urls.txt

echo "Calculate sha256 for all Sources files...";

rm *.sha256

for files in *; do
  sha256sum $files > $files.sha256
  echo "1. [$files]($blob_url/$files) - [SHA256]($blob_url/$files.sha256)" >> ../README.md;
done

if [[ "$?" == "0" ]]; then
    rm ../README.md.old;
    echo "Done.";
else
    mv ../README.md.old README.md;
    echo "Fail.";
fi
