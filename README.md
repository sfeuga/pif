# Post Install Files

### Install files use to deploy my workstation.

To download latest files and update the README:

1. Install [Git LFS](https://git-lfs.github.com/).
2. Change urls in the `Tools/urls.txt` (one url per line) with new one. **Attention**: Some URL are redirected (like on github) and it will not work, use "the last redirected URL".
3. Run `Tools/update_readme_and_dl_files.sh`.

Files available in this repo:

1. [amdgpu-pro-20.10-1048554-rhel-7.8.tar.xz](https://gitlab.com/sfeuga/pif/-/raw/master/Sources/amdgpu-pro-20.10-1048554-rhel-7.8.tar.xz) - [SHA256](https://gitlab.com/sfeuga/pif/-/raw/master/Sources/amdgpu-pro-20.10-1048554-rhel-7.8.tar.xz.sha256)
1. [amdgpu-pro-20.10-1048554-rhel-8.1.tar.xz](https://gitlab.com/sfeuga/pif/-/raw/master/Sources/amdgpu-pro-20.10-1048554-rhel-8.1.tar.xz) - [SHA256](https://gitlab.com/sfeuga/pif/-/raw/master/Sources/amdgpu-pro-20.10-1048554-rhel-8.1.tar.xz.sha256)
1. [amdgpu-pro-20.20-1089974-rhel-8.2.tar.xz](https://gitlab.com/sfeuga/pif/-/raw/master/Sources/amdgpu-pro-20.20-1089974-rhel-8.2.tar.xz) - [SHA256](https://gitlab.com/sfeuga/pif/-/raw/master/Sources/amdgpu-pro-20.20-1089974-rhel-8.2.tar.xz.sha256)
1. [Autodesk_Maya_2019_ML_Linux_64bit_Hack.zip](https://gitlab.com/sfeuga/pif/-/raw/master/Sources/Autodesk_Maya_2019_ML_Linux_64bit_Hack.zip) - [SHA256](https://gitlab.com/sfeuga/pif/-/raw/master/Sources/Autodesk_Maya_2019_ML_Linux_64bit_Hack.zip.sha256)
1. [Autodesk_Maya_2020_ML_Linux_64bit_Hack.zip](https://gitlab.com/sfeuga/pif/-/raw/master/Sources/Autodesk_Maya_2020_ML_Linux_64bit_Hack.zip) - [SHA256](https://gitlab.com/sfeuga/pif/-/raw/master/Sources/Autodesk_Maya_2020_ML_Linux_64bit_Hack.zip.sha256)
1. [Bifrost2019-2.1.0.0-1.x86_64.rpm](https://gitlab.com/sfeuga/pif/-/raw/master/Sources/Bifrost2019-2.1.0.0-1.x86_64.rpm) - [SHA256](https://gitlab.com/sfeuga/pif/-/raw/master/Sources/Bifrost2019-2.1.0.0-1.x86_64.rpm.sha256)
1. [Bifrost2020-2.1.0.0-1.x86_64.rpm](https://gitlab.com/sfeuga/pif/-/raw/master/Sources/Bifrost2020-2.1.0.0-1.x86_64.rpm) - [SHA256](https://gitlab.com/sfeuga/pif/-/raw/master/Sources/Bifrost2020-2.1.0.0-1.x86_64.rpm.sha256)
1. [Bifrost_2.0.5.1_Maya2019_Linux.run](https://gitlab.com/sfeuga/pif/-/raw/master/Sources/Bifrost_2.0.5.1_Maya2019_Linux.run) - [SHA256](https://gitlab.com/sfeuga/pif/-/raw/master/Sources/Bifrost_2.0.5.1_Maya2019_Linux.run.sha256)
1. [Bifrost_2.0.5.1_Maya2020_Linux.run](https://gitlab.com/sfeuga/pif/-/raw/master/Sources/Bifrost_2.0.5.1_Maya2020_Linux.run) - [SHA256](https://gitlab.com/sfeuga/pif/-/raw/master/Sources/Bifrost_2.0.5.1_Maya2020_Linux.run.sha256)
1. [Bifrost_2.1.0.0_Maya2019_Linux.run](https://gitlab.com/sfeuga/pif/-/raw/master/Sources/Bifrost_2.1.0.0_Maya2019_Linux.run) - [SHA256](https://gitlab.com/sfeuga/pif/-/raw/master/Sources/Bifrost_2.1.0.0_Maya2019_Linux.run.sha256)
1. [Bifrost_2.1.0.0_Maya2020_Linux.run](https://gitlab.com/sfeuga/pif/-/raw/master/Sources/Bifrost_2.1.0.0_Maya2020_Linux.run) - [SHA256](https://gitlab.com/sfeuga/pif/-/raw/master/Sources/Bifrost_2.1.0.0_Maya2020_Linux.run.sha256)
1. [epson-inkjet-printer-escpr-1.6.41-2.1lsb3.2.fc31.x86_64.rpm](https://gitlab.com/sfeuga/pif/-/raw/master/Sources/epson-inkjet-printer-escpr-1.6.41-2.1lsb3.2.fc31.x86_64.rpm) - [SHA256](https://gitlab.com/sfeuga/pif/-/raw/master/Sources/epson-inkjet-printer-escpr-1.6.41-2.1lsb3.2.fc31.x86_64.rpm.sha256)
1. [epson-inkjet-printer-escpr_1.7.7-1lsb3.2_amd64.deb](https://gitlab.com/sfeuga/pif/-/raw/master/Sources/epson-inkjet-printer-escpr_1.7.7-1lsb3.2_amd64.deb) - [SHA256](https://gitlab.com/sfeuga/pif/-/raw/master/Sources/epson-inkjet-printer-escpr_1.7.7-1lsb3.2_amd64.deb.sha256)
1. [epson-inkjet-printer-escpr-1.7.7-1lsb3.2.src.rpm](https://gitlab.com/sfeuga/pif/-/raw/master/Sources/epson-inkjet-printer-escpr-1.7.7-1lsb3.2.src.rpm) - [SHA256](https://gitlab.com/sfeuga/pif/-/raw/master/Sources/epson-inkjet-printer-escpr-1.7.7-1lsb3.2.src.rpm.sha256)
1. [epson-inkjet-printer-escpr-1.7.7-1lsb3.2.x86_64.rpm](https://gitlab.com/sfeuga/pif/-/raw/master/Sources/epson-inkjet-printer-escpr-1.7.7-1lsb3.2.x86_64.rpm) - [SHA256](https://gitlab.com/sfeuga/pif/-/raw/master/Sources/epson-inkjet-printer-escpr-1.7.7-1lsb3.2.x86_64.rpm.sha256)
1. [epson-printer-utility_1.1.1-1lsb3.2_amd64.deb](https://gitlab.com/sfeuga/pif/-/raw/master/Sources/epson-printer-utility_1.1.1-1lsb3.2_amd64.deb) - [SHA256](https://gitlab.com/sfeuga/pif/-/raw/master/Sources/epson-printer-utility_1.1.1-1lsb3.2_amd64.deb.sha256)
1. [epson-printer-utility-1.1.1-1lsb3.2.src.rpm](https://gitlab.com/sfeuga/pif/-/raw/master/Sources/epson-printer-utility-1.1.1-1lsb3.2.src.rpm) - [SHA256](https://gitlab.com/sfeuga/pif/-/raw/master/Sources/epson-printer-utility-1.1.1-1lsb3.2.src.rpm.sha256)
1. [epson-printer-utility-1.1.1-1lsb3.2.x86_64.rpm](https://gitlab.com/sfeuga/pif/-/raw/master/Sources/epson-printer-utility-1.1.1-1lsb3.2.x86_64.rpm) - [SHA256](https://gitlab.com/sfeuga/pif/-/raw/master/Sources/epson-printer-utility-1.1.1-1lsb3.2.x86_64.rpm.sha256)
1. [hdRpr-1.2.2-Houdini-18.0.460-linux-package.zip](https://gitlab.com/sfeuga/pif/-/raw/master/Sources/hdRpr-1.2.2-Houdini-18.0.460-linux-package.zip) - [SHA256](https://gitlab.com/sfeuga/pif/-/raw/master/Sources/hdRpr-1.2.2-Houdini-18.0.460-linux-package.zip.sha256)
1. [imagescan-3.63.0-1epson4centos8.src.rpm](https://gitlab.com/sfeuga/pif/-/raw/master/Sources/imagescan-3.63.0-1epson4centos8.src.rpm) - [SHA256](https://gitlab.com/sfeuga/pif/-/raw/master/Sources/imagescan-3.63.0-1epson4centos8.src.rpm.sha256)
1. [imagescan-3.63.0-1epson4fedora32.src.rpm](https://gitlab.com/sfeuga/pif/-/raw/master/Sources/imagescan-3.63.0-1epson4fedora32.src.rpm) - [SHA256](https://gitlab.com/sfeuga/pif/-/raw/master/Sources/imagescan-3.63.0-1epson4fedora32.src.rpm.sha256)
1. [imagescan-bundle-centos-8-3.63.0.x64.rpm.tar.gz](https://gitlab.com/sfeuga/pif/-/raw/master/Sources/imagescan-bundle-centos-8-3.63.0.x64.rpm.tar.gz) - [SHA256](https://gitlab.com/sfeuga/pif/-/raw/master/Sources/imagescan-bundle-centos-8-3.63.0.x64.rpm.tar.gz.sha256)
1. [imagescan-bundle-fedora-32-3.63.0.x64.rpm.tar.gz](https://gitlab.com/sfeuga/pif/-/raw/master/Sources/imagescan-bundle-fedora-32-3.63.0.x64.rpm.tar.gz) - [SHA256](https://gitlab.com/sfeuga/pif/-/raw/master/Sources/imagescan-bundle-fedora-32-3.63.0.x64.rpm.tar.gz.sha256)
1. [intel_sdk_for_opencl_applications_2020.1.395.tar.gz](https://gitlab.com/sfeuga/pif/-/raw/master/Sources/intel_sdk_for_opencl_applications_2020.1.395.tar.gz) - [SHA256](https://gitlab.com/sfeuga/pif/-/raw/master/Sources/intel_sdk_for_opencl_applications_2020.1.395.tar.gz.sha256)
1. [l_opencl_p_18.1.0.015.tgz](https://gitlab.com/sfeuga/pif/-/raw/master/Sources/l_opencl_p_18.1.0.015.tgz) - [SHA256](https://gitlab.com/sfeuga/pif/-/raw/master/Sources/l_opencl_p_18.1.0.015.tgz.sha256)
1. [MayaBonusTools-2017-2020-linux.sh](https://gitlab.com/sfeuga/pif/-/raw/master/Sources/MayaBonusTools-2017-2020-linux.sh) - [SHA256](https://gitlab.com/sfeuga/pif/-/raw/master/Sources/MayaBonusTools-2017-2020-linux.sh.sha256)
1. [megacmd_1.2.0-6.1_armhf.deb](https://gitlab.com/sfeuga/pif/-/raw/master/Sources/megacmd_1.2.0-6.1_armhf.deb) - [SHA256](https://gitlab.com/sfeuga/pif/-/raw/master/Sources/megacmd_1.2.0-6.1_armhf.deb.sha256)
1. [megacmd-1.2.0-8.1.x86_64.rpm](https://gitlab.com/sfeuga/pif/-/raw/master/Sources/megacmd-1.2.0-8.1.x86_64.rpm) - [SHA256](https://gitlab.com/sfeuga/pif/-/raw/master/Sources/megacmd-1.2.0-8.1.x86_64.rpm.sha256)
1. [megasync-4.3.1-1.1.x86_64.rpm](https://gitlab.com/sfeuga/pif/-/raw/master/Sources/megasync-4.3.1-1.1.x86_64.rpm) - [SHA256](https://gitlab.com/sfeuga/pif/-/raw/master/Sources/megasync-4.3.1-1.1.x86_64.rpm.sha256)
1. [megasync_4.3.1-3.1_armhf.deb](https://gitlab.com/sfeuga/pif/-/raw/master/Sources/megasync_4.3.1-3.1_armhf.deb) - [SHA256](https://gitlab.com/sfeuga/pif/-/raw/master/Sources/megasync_4.3.1-3.1_armhf.deb.sha256)
1. [nautilus-megasync_1.1.0_armhf.deb](https://gitlab.com/sfeuga/pif/-/raw/master/Sources/nautilus-megasync_1.1.0_armhf.deb) - [SHA256](https://gitlab.com/sfeuga/pif/-/raw/master/Sources/nautilus-megasync_1.1.0_armhf.deb.sha256)
1. [nautilus-megasync-3.6.6-2.1.x86_64.rpm](https://gitlab.com/sfeuga/pif/-/raw/master/Sources/nautilus-megasync-3.6.6-2.1.x86_64.rpm) - [SHA256](https://gitlab.com/sfeuga/pif/-/raw/master/Sources/nautilus-megasync-3.6.6-2.1.x86_64.rpm.sha256)
1. [RadeonProRenderBlender_Ubuntu.zip](https://gitlab.com/sfeuga/pif/-/raw/master/Sources/RadeonProRenderBlender_Ubuntu.zip) - [SHA256](https://gitlab.com/sfeuga/pif/-/raw/master/Sources/RadeonProRenderBlender_Ubuntu.zip.sha256)
1. [radeonprorenderformaya.run](https://gitlab.com/sfeuga/pif/-/raw/master/Sources/radeonprorenderformaya.run) - [SHA256](https://gitlab.com/sfeuga/pif/-/raw/master/Sources/radeonprorenderformaya.run.sha256)
1. [radeonprorendermateriallibraryinstaller.run](https://gitlab.com/sfeuga/pif/-/raw/master/Sources/radeonprorendermateriallibraryinstaller.run) - [SHA256](https://gitlab.com/sfeuga/pif/-/raw/master/Sources/radeonprorendermateriallibraryinstaller.run.sha256)
1. [vtune_profiler_2020_update1.tar.gz](https://gitlab.com/sfeuga/pif/-/raw/master/Sources/vtune_profiler_2020_update1.tar.gz) - [SHA256](https://gitlab.com/sfeuga/pif/-/raw/master/Sources/vtune_profiler_2020_update1.tar.gz.sha256)
1. [ykpers-1.19.0.tar.gz](https://gitlab.com/sfeuga/pif/-/raw/master/Sources/ykpers-1.19.0.tar.gz) - [SHA256](https://gitlab.com/sfeuga/pif/-/raw/master/Sources/ykpers-1.19.0.tar.gz.sha256)
1. [yubico-piv-tool-latest.tar.gz](https://gitlab.com/sfeuga/pif/-/raw/master/Sources/yubico-piv-tool-latest.tar.gz) - [SHA256](https://gitlab.com/sfeuga/pif/-/raw/master/Sources/yubico-piv-tool-latest.tar.gz.sha256)
1. [yubikey-manager-qt-latest-linux.AppImage](https://gitlab.com/sfeuga/pif/-/raw/master/Sources/yubikey-manager-qt-latest-linux.AppImage) - [SHA256](https://gitlab.com/sfeuga/pif/-/raw/master/Sources/yubikey-manager-qt-latest-linux.AppImage.sha256)
1. [yubikey-personalization-gui-3.1.25.tar.gz](https://gitlab.com/sfeuga/pif/-/raw/master/Sources/yubikey-personalization-gui-3.1.25.tar.gz) - [SHA256](https://gitlab.com/sfeuga/pif/-/raw/master/Sources/yubikey-personalization-gui-3.1.25.tar.gz.sha256)
1. [yubioath-desktop-latest-linux.AppImage](https://gitlab.com/sfeuga/pif/-/raw/master/Sources/yubioath-desktop-latest-linux.AppImage) - [SHA256](https://gitlab.com/sfeuga/pif/-/raw/master/Sources/yubioath-desktop-latest-linux.AppImage.sha256)
